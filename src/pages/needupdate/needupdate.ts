import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import {NavController, NavParams} from 'ionic-angular';


@Component({
  selector: 'page-needupdatepage',
  templateUrl: 'needupdate.html'
})
export class NeedUpdatePage {
  @ViewChild(Nav) nav: Nav;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  nextPage() {
  }
  closePage(){
    this.navCtrl.pop();
  }
}
