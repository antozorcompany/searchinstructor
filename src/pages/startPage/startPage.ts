import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { UserTypeSelectPage } from '../usertypeselect/usertypeselect'

@Component({
  selector: 'page-startpage',
  templateUrl: 'startPage.html'
})
/** Страртовая страница приложения */
export class StartPage {
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen) {
    this.splashScreen.hide();
  }

  openPage() {
    this.navCtrl.setRoot(UserTypeSelectPage);
  }
}
