import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import {Keyboard} from "@ionic-native/keyboard";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HTTPResponse } from '@ionic-native/http';

import { HttpService } from '../../app/httpService'
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { EndRegistrationInstructor } from '../endRegistrationInstructor/endRegistrationInstructor';
import { EndRegistrationStudent } from '../endRegistrationStudent/endRegistrationStudent';

@Component({
  selector: 'page-checkphone',
  templateUrl: 'checkPhone.html',
  providers: [HttpService]
})
export class CheckPhone {
  @ViewChild(Nav) nav: Nav;
  private RegisterInstructor: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
    private http: HttpService, private globalProvider: GlobalProvider, private validateWindow: ValidationWindows,
    public keyboard: Keyboard) {
    this.RegisterInstructor = this.formBuilder.group({
      code: ['', Validators.required]
    });
  }
  submitAgain() {
    let body = {
        'phone': this.navParams.get("data").tel
    };
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/resendcode', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        console.log('ok')
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  sendCode() {
    let body = {
        'phone': this.navParams.get("data").tel,
        'code': this.RegisterInstructor.value.code
    };
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/confirm', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let body = res.data;
        if(body == 'error') {
          this.validateWindow.showValidate('Не верно введен код из СМС.');
        }
        else {
          if(this.navParams.get("data").type == "instructor") {
            localStorage.setItem("Session", "instructor");
            this.navCtrl.setRoot(EndRegistrationInstructor);
          } else {
            localStorage.setItem("Session", "student");
            this.navCtrl.setRoot(EndRegistrationStudent);
          }
        }
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err);
      });
  }
  closeKeyboard() {
    this.keyboard.hide();
  }
}
