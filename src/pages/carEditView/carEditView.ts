import {Component, ViewChild, Renderer2} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import {ValidationWindows} from "../../app/validationWindows";
import {CheckPhone} from "../checkPhone/checkPhone";
import {QuestionWindow} from "../../app/questionWindow";
import { Car } from '../../modules/addAuto/addAuto';

@Component({
  selector: 'page-careditview',
  templateUrl: 'carEditView.html',
})
export class CarEditView {
  @ViewChild(Nav) nav: Nav;

  callback: any = null;
  cars: Car[] = [];
  editMode: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private renderer2: Renderer2,
    private validateWindow: ValidationWindows, private questionWindow: QuestionWindow) {
    this.cars = Object.assign([], this.navParams.get("cars"));
    if (this.navParams.get("type") == 'edit') {
      this.editMode = true;
      this.callback = this.navParams.get("callback")
    }
  }
  delete(index){
    // Если машина не валидная сразу ее удалим. Значит мы отменили редактирование
    if(!this.isValid(this.cars[index])) {
      this.cars.splice(index,1);
      // Вернем их обратно
      let buttons = document.getElementsByClassName('carEditButtons');
      this.renderer2.removeClass(buttons[0], 'showHide');
    }
    else
      this.questionWindow.showQuestion('Вы действительно хотите удалить машину?', function(){
        this.cars.splice(index,1);
      }, function(){}, this);
  }
  isValid(car) {
    return car.manufacturer && car.gearbox && car.vehicle_type && car.name;
  }
  addAuto(){
    let c = new Car();
    c.photo = 'assets/imgs/tachka.svg';
    c.immideatelyEdit = true;
    this.cars.push(c);
    //Скороем кнопки иначе на иосе они остануться видны
    let buttons = document.getElementsByClassName('carEditButtons');
    this.renderer2.addClass(buttons[0], 'showHide');
  }
  addAutoValue(value, i) {
    let id = this.cars[i].id;
    this.cars[i] = value;
    this.cars[i].id = id;
    // Вернем их обратно
    let buttons = document.getElementsByClassName('carEditButtons');
    this.renderer2.removeClass(buttons[0], 'showHide');
  }
  backToProfile() {
    if(this.cars.length == 0) {
      this.validateWindow.showValidate('Необходимо добавить хотябы одну машину.');
      return;
    }
    if(this.callback)
      this.callback(this.cars);
    this.navCtrl.pop();
  }
  //TODO: а зачем это тут?
  checkPhone(){
    this.navCtrl.setRoot(CheckPhone, {
      "data": {
        "tel": this.navParams.get("data").registration1.tel,
        "type": "instructor"
      }
    });
  }
}
