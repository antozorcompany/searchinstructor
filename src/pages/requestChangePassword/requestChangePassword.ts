import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import {Keyboard} from "@ionic-native/keyboard";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HTTPResponse } from '@ionic-native/http';

import { HttpService } from '../../app/httpService'
import { ChangePassword } from "../changePassword/changePassword";
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';


@Component({
  selector: 'page-requestChangePassword',
  templateUrl: 'requestChangePassword.html',
  providers: [HttpService]
})
export class RequestChangePassword {
  @ViewChild(Nav) nav: Nav;
  private RegisterInstructor: FormGroup;
  public mask = ['+','7','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
    private http: HttpService, private globalProvider: GlobalProvider, private validateWindow: ValidationWindows,
    public keyboard: Keyboard) {
    this.RegisterInstructor = this.formBuilder.group({
      tel: ['', Validators.required]
    });
  }
  sendCode() {
    let tel = this.RegisterInstructor.value.tel;
    tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    let body = {
        'phone': tel
    }
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/requestchangepassword', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let resbody = res.data;
        if(resbody == 'error') {
          this.validateWindow.showValidate('Не найдена учетная запись для введенного номера телефона.');
          return;
        }
        this.navCtrl.push(ChangePassword, {
          "data": {
            "tel": tel
          }
        });
      })
      .catch((err:HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  focusMask(phone){
    phone.placeholder = "+7(___) ___-____";
  }
  closePage(){
    this.navCtrl.pop();
  }
  closeKeyboard() {
    this.keyboard.hide();
  }
}
