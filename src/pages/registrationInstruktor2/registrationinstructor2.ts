import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams, normalizeURL} from 'ionic-angular';
import { RegistrationInstructor3Page } from "../registrationInstruktor3/registrationinstructor3";
import {HttpService} from "../../app/httpService";
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import {PhotoSelectorWindow} from "../../app/photoSelectorWindow";
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-registrationinstructor2',
  templateUrl: 'registrationinstructor2.html',
})
export class RegistrationInstructor2Page {
  @ViewChild(Nav) nav: Nav;
  birthday: any[] = [];
  start_teaching: any[] = [];
  birthdayString: string;
  start_teachingString: string;
  allCheck: boolean = false;
  result: object = {'birthYear': null, 'startYear': null, 'sex': null, 'city' : null};
  cities: string = JSON.stringify([]);
  sexs: string = JSON.stringify([{'title':'Мужской','id':'male'},{'title':'Женский','id':'female'}]);
  cityAlias: string = '';
  ageAlias: string = '';
  experienceAlias: string = '';
  defaultImageSrc: string = "assets/imgs/fotik.svg";
  imageSrc: SafeUrl = this.defaultImageSrc;
  fileUri:string = this.defaultImageSrc;
  constructor(public navCtrl: NavController, private http: HttpService, private geolocation: Geolocation,
    private camera: Camera, private nativeGeocoder: NativeGeocoder, public navParams: NavParams,
    private photoSelectorWindow: PhotoSelectorWindow, private ds: DomSanitizer, private crop: Crop) {
    this.imageSrc = navParams.get('data').imageSrc;
    this.fileUri = navParams.get('data').fileUri;
    let year = new Date().getFullYear();
    for (let i = year-18-3; i >= (year-60); i--){
      this.birthday.push({'title': i, 'id': i});
    }
    for (let i = year-3; i >= (year-42); i--){
      this.start_teaching.push({'title': i, 'id': i});
    }
    this.start_teachingString = JSON.stringify(this.start_teaching);
    this.birthdayString = JSON.stringify(this.birthday);
    this.loadCities();
  }
  onSelectChangeBirth(selectedValue: any) {
    this.result['birthYear'] = selectedValue;
    this.ageAlias = selectedValue + ' г.р.';
    this.allCheck = this.isValid();
  }
  onSelectChangeStart(selectedValue: any) {
    this.result['startYear'] = selectedValue;
    this.experienceAlias = 'c ' + selectedValue + ' г.';
    this.allCheck = this.isValid();
  }
  onSelectChangedSex(selectedValue: any){
    this.result['sex'] = selectedValue;
    this.allCheck = this.isValid();
  }
  onSelectChangedCity(selectedValue: any){
    this.result['city'] = selectedValue;
    this.allCheck = this.isValid();
  }
  isValid() {
    return this.result['birthYear'] != null && this.result['startYear'] != null && this.result['sex'] != null && this.result['city'] != null;
  }
  openPage(page) {
    if (page == 'next'){
      if (this.result['birthYear'] != null && this.result['startYear'] != null)
        this.navCtrl.push(RegistrationInstructor3Page, {
          "data": {
            "registration1": this.navParams.get("data"),
            "registration2": this.result,
            "imageSrc": this.imageSrc,
            'fileUri': this.fileUri
          }
        });
    }
  }
  closePage(){
    this.navCtrl.pop();
  }
  onChanged(id){
    console.log(id);
  }
  loadCities() {
    this.http.getData('/region/city')
      .then((res: HTTPResponse) => {
        let body = JSON.parse(res.data);
        let data = [];
        for(let city of body) {
          data.push({
            'title': city.name,
            'id': city.id
          })
        }
        this.cities = JSON.stringify(data);
        this.findGeolocation();
      })
      .catch((err: HTTPResponse) => {
        console.log("error");
        console.log(err.error);
      });
  }
  findGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      //57.867766, 39.531339 - тутаев
      //59.874746, 30.390485 - питер
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          let locality = result[0].locality;
          let cities = JSON.parse(this.cities);
          for(let i = 0; i < cities.length; i++) {
            if(cities[i].title == locality) {
              this.result['city'] = cities[i].id;
              this.cityAlias = cities[i].title;
              break;
            }
          }
        })
        .catch((error: any) => console.log(error));
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  selectPhotoType() {
    this.photoSelectorWindow.showWindow(this.selectFromGalary, this.makePhoto, this);
  }
  selectFromGalary() {
    this.selectPhoto(true);
  }
  makePhoto() {
    this.selectPhoto(false);
  }
  selectPhoto(galary:boolean) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.FRONT,
      correctOrientation: true
    };
    if(galary)
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.camera.getPicture(options)
      .then((file_uri) => {
        this.crop.crop(file_uri, { quality: 100, targetWidth: -1, targetHeight: -1 })
          .then((path) => {
            path = normalizeURL(path);
            this.imageSrc = this.ds.bypassSecurityTrustUrl(path);
            this.fileUri = path;
          })
      })
  }
}
