import { Component, Renderer2 } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { ProfileInstructorForStudent } from '../profileInstructorForStudent/profileInstructorForStudent'
import {HttpService} from "../../app/httpService";
import { ValidationWindows } from '../../app/validationWindows';
import {UserTypeSelectPage} from "../usertypeselect/usertypeselect";
import {QuestionWindow} from '../../app/questionWindow';
import {Cookie} from "ng2-cookies";
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-resultsearch',
  templateUrl: 'resultseartch.html',
})
export class ResultSearchPage {
  data: any[];
  page: number = 1;
  pages: number = 1;
  body: any;
  scrollBlock: any;
  serverAddr: string = '';
  logoutsrc: string = '';
  constructor(public renderer: Renderer2,public navCtrl: NavController, public navParams: NavParams,
    private http: HttpService, private validateWindow: ValidationWindows, private questionWindow: QuestionWindow) {
    this.serverAddr = http.urlServer;
    if(localStorage.getItem('Session') == 'student')
      this.logoutsrc = '';
    this.pages = this.navParams.get('data').pages;
    this.body = this.navParams.get('body');
    this.data = this.navParams.get('data').data;
    let i = 0;
    for(i; i < this.data.length; i++) {
        if(this.data[i].gearbox == 'automatic')
          this.data[i].gearbox = 'АКПП';
        else
          this.data[i].gearbox = 'МКПП';
    }
  }

  scroll = (): void => {
    let scrollBlocks = document.getElementById('all-instructor-items');
    if(scrollBlocks.scrollHeight- window.innerHeight - scrollBlocks.scrollTop < 100){
      if(this.page < this.pages){
        // Будет добавлять loader только если его нет. Иногда возникает какой-то баг что оно не исчезает
        if(this.data[this.data.length - 1] != 'loader')
          this.data.push('loader')
        this.page+=1;
        this.body['page'] = this.page;
        this.http.postData('/instructor/search', this.body)
          .then((res: HTTPResponse)=>{
            let data = JSON.parse(res.data).data;
            for(let i = 0; i < data.length; i++) {
              if(data[i].gearbox == 'automatic')
                data[i].gearbox = 'АКПП';
              else
                data[i].gearbox = 'МКПП';
            }
            if(this.data[this.data.length - 1] == 'loader')
              this.data.splice(this.data.length-1, 1);
            this.data = this.data.concat(data);
          })
          .catch((err: HTTPResponse) => {
            let errcode = err.status;
            switch(errcode) {
              case 0:
                this.validateWindow.showConnectionLostMessage();
                break;
              case 500:
                this.validateWindow.showServerErrorMessage();
                break;
              default:
                this.validateWindow.showUnknownErrorMessage();
            }
            console.log("error");
            console.log(err.error);
          });
      }
    }
  };
  closePage(){
    this.navCtrl.pop();
  }
  openPage(id){
    this.navCtrl.push(ProfileInstructorForStudent, {"instructorId":id, 'callback':this.onShowBack});
  }
  onShowBack = (id, likes, dislikes) => {
    for(let instructor of this.data) {
      if(instructor.id == id) {
        instructor.likes = likes;
        instructor.dislikes = dislikes;
      }
    }
  };
  logout(){
    this.questionWindow.showQuestion('Вы действительно хотите выйти?', function(){
      /*this.http.postData('/account/clearfcmtoken', {})
        .then((res: HTTPResponse) => {*/
          this.http.getData('/logout')
            .then((res: HTTPResponse) => {
              localStorage.removeItem('Session');
              Cookie.delete('carinstructorfinder.sid');
              this.navCtrl.setRoot(UserTypeSelectPage);
            });
        //});
      }, function(){}, this);
  }
}
