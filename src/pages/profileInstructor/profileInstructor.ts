import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { EditProfileInstructor } from '../editProfileInstructor/editProfileInstructor'
import {CarEditView} from "../carEditView/carEditView";
import { HttpService } from '../../app/httpService'
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { Comments } from '../comments/comments';
import { Car } from '../../modules/addAuto/addAuto';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-profileinstructor',
  templateUrl: 'profileInstructor.html'
})
export class ProfileInstructor {
  birthYear: number;
  fio: string;
  phone: string;
  startYear: number;
  sex: string;
  cars: Car[] = [];
  data: any;
  city:string;
  likes: number;
  reviews: number;
  dislikes: number;
  reviewsFormat: string;
  instructorId: number;
  photoSrc: string = 'assets/imgs/chel.svg';
  profileLoaded: boolean = false;
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen, public navParams: NavParams,
    private http: HttpService, private globalProvider: GlobalProvider, private validateWindow: ValidationWindows) {
    this.splashScreen.hide();
    this.loadProfile();
  }
  stringPhone(numberPhone){
    let stringPhone = '';
    for(let i = 0; i < numberPhone.length; i++){
      if(i == 0){
        stringPhone += '+';
      }
      if(i==1){
        stringPhone+= '(';
      }
      if(i==4){
        stringPhone+= ') ';
      }
      if(i == 7){
        stringPhone+='-'
      }
      stringPhone+=numberPhone[i];
    }
    return stringPhone;
  }
  openPage(page) {
    if(page == 'profile') {
      // this.http.postData('/instructor/profile', {}).toPromise().then((res: Response)=>{console.log('succses');console.log(res);}).catch(err=>{
      //   console.log("error"); console.log(err)});
      // this.navCtrl.push(StudentStartPage);
      // this.navCtrl.setRoot(StudentStartPage);
    }
  }
  edit(){
    console.log(this.data);
    this.navCtrl.push(EditProfileInstructor, {'res': this.data});
  }
  viewAuto(){
    this.navCtrl.push(CarEditView, {'cars': this.cars, 'type': 'view'})
  }
  loadProfile() {
    let loading = this.globalProvider.createLoadingController();
    loading.present();

    this.http.postData('/instructor/profile', {})
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let data = JSON.parse(res.data);
        this.data = data;
        this.instructorId = data.id;
        this.birthYear = data.birthyear;
        this.startYear = data.startworkyear;
        this.phone = this.stringPhone(data.phone);
        this.fio = data.fio;
        this.sex = data.sex == 'male'?'Мужской':'Женский';
        this.city = data.cityname;
        this.likes = data.likes;
        this.dislikes = data.dislikes;
        this.reviews = data.reviews;
        this.reviewsFormat = this.globalProvider.getReviewFormat(this.reviews);
        for(let c of data.cars) {
          let car = new Car(c.id, c.name, c.vehicle_type, c.gearbox, c.manufacturer, c.photo_url);
          if(car.photo)
            car.photo = this.http.urlServer + car.photo + '?t=' + (new Date()).getTime();
          else
            car.photo = 'assets/imgs/tachka.svg';
          this.cars.push(car);
        }
        if(data.photo_md5)
            this.photoSrc = this.http.urlServer + data.photo_url + '?t=' + (new Date()).getTime();
        this.profileLoaded = true;
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  viewReviews(){
    this.navCtrl.push(Comments, {id:this.instructorId})
  }
}
