import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import {NavController, NavParams} from 'ionic-angular';

import { SearchinstructorPage} from '../searchinstructor/searchinstructor';


@Component({
  selector: 'page-studentauthpage',
  templateUrl: 'studentauth.html'
})
/** Страница отображаемая ученику при запуске приложения когда он уже был авторизован ранее */
export class StudentAuthPage {
  @ViewChild(Nav) nav: Nav;
  username: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.username = this.navParams.get('username');
  }
  nextPage() {
      this.navCtrl.setRoot(SearchinstructorPage, {'back': false});
  }
  closePage(){
    this.navCtrl.pop();
  }
}
