import {Component, ViewChild, Renderer, ElementRef} from '@angular/core';
import {Nav, NavController, normalizeURL} from 'ionic-angular';
import { HTTPResponse } from '@ionic-native/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { RegistrationInstructor2Page } from "../registrationInstruktor2/registrationinstructor2"
import {Keyboard} from "@ionic-native/keyboard";
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {HttpService} from "../../app/httpService";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import {PhotoSelectorWindow} from "../../app/photoSelectorWindow";
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { CheckPhone } from '../checkPhone/checkPhone';

@Component({
  selector: 'page-registrationinstructor1',
  templateUrl: 'registrationinstructor1.html',
})
export class RegistratIoninstructor1Page {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('phone') input1: ElementRef;
  @ViewChild('passwordItem') input2: ElementRef;
  @ViewChild('confirmItem') input3: ElementRef;

  private RegisterInstructor: FormGroup;
  public mask = ['+','7','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  defaultImageSrc: string = "assets/imgs/fotik.svg";
  imageSrc: SafeUrl = this.defaultImageSrc;
  fileUri:string = this.defaultImageSrc;
  constructor(public navCtrl: NavController, public keyboard: Keyboard, private formBuilder: FormBuilder,
    private camera: Camera, private http: HttpService, private photoSelectorWindow: PhotoSelectorWindow,
    private ds: DomSanitizer, private globalProvider: GlobalProvider, private crop: Crop,
    private validateWindow: ValidationWindows, private renderer: Renderer) {
    this.RegisterInstructor = this.formBuilder.group({
      tel: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  focusMask(phone){
      phone.placeholder = "+7(___) ___-____";
  }
  focusNext(number){
    switch(number) {
      case 1:
        this.renderer.invokeElementMethod(this.input1.nativeElement, 'focus');
        break;
      case 2:
        this.renderer.invokeElementMethod(this.input2.nativeElement, 'focus');
        break;
      case 3:
        if(this.checkPassword())
          this.renderer.invokeElementMethod(this.input3.nativeElement, 'focus');
        break;
      case 4:
        this.keyboard.hide();
        break;
    }
  }
  openPage(page) {
    if (page == 'next') {
      if(!this.checkPassword())
        return;
      if (this.RegisterInstructor.value.password != this.RegisterInstructor.value.confirmPassword) {
        this.validateWindow.showValidate('Пароль и подтверждение не совпадают.');
        return;
      }
      let tel = this.RegisterInstructor.value.tel;
      tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');

      let loading = this.globalProvider.createLoadingController();
      loading.present();

      this.http.postData('/account/checkphone', {
        "phone": tel
      })
      .then((res: HTTPResponse) => {
        loading.dismiss();
        this.checkPhone(res);
        console.log('then');
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error)
      });
    }
  }
  hasNumber(str) {
    return /\d/.test(str);
  }
  hasLetter(str) {
    return /[a-zA-Z]/.test(str);
  }
  checkPassword(){
    let password = this.RegisterInstructor.value.password;
    if(password.length < 6 || !this.hasNumber(password) || !this.hasLetter(password)) {
      this.validateWindow.showValidate('Пароль должен состоять из цифр, латинских букв и содержать не менее 6 символов.');
      return false;
    }
    return true;
  }
  checkPhone(Res: HTTPResponse){
    let res = JSON.parse(Res.data)
    let tel = this.RegisterInstructor.value.tel;
    tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    if(res.status == 'ok'){
      this.navCtrl.push(RegistrationInstructor2Page, {
        "data": {
          "tel": tel,
          'name':this.RegisterInstructor.value.name,
          'password': this.RegisterInstructor.value.password,
          'imageSrc': this.imageSrc,
          'fileUri': this.fileUri
        }
      });
    }
    else if(res.status == 'unconfirmed') {
      this.validateWindow.showValidate('Для введенного номера телефона есть неподтвержденная учетная запись. Необходимо подтверждение.', function() {
        this.http.postData('/account/resendcode', {'phone': tel})
          .then((res: HTTPResponse) => {
            this.navCtrl.setRoot(CheckPhone, {
              "data": {
                "tel": tel,
                "type": "instructor"
              }
            });
          })
      }, this);
    } else {
      this.validateWindow.showValidate('Для введенного номера телефона уже есть учетная запись.');
    }
  }
  selectPhotoType() {
    this.photoSelectorWindow.showWindow(this.selectFromGalary, this.makePhoto, this);
  }
  selectFromGalary() {
    this.selectPhoto(true);
  }
  makePhoto() {
    this.selectPhoto(false);
  }
  selectPhoto(galary:boolean) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.FRONT,
      correctOrientation: true
    };
    if(galary)
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.camera.getPicture(options)
      .then((file_uri) => {
        this.crop.crop(file_uri, { quality: 100, targetWidth: -1, targetHeight: -1 })
          .then((path) => {
            path = normalizeURL(path);
            this.imageSrc = this.ds.bypassSecurityTrustUrl(path);
            this.fileUri = path;
          })
      })
  }
  closePage(){
    this.navCtrl.pop();
  }
}
