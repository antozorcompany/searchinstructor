import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import {NavController, NavParams} from 'ionic-angular';
import { ProfileInstructor} from '../profileInstructor/profileInstructor';

@Component({
  selector: 'page-instructorauth',
  templateUrl: 'instructorauth.html'
})
export class InstructorAuthPage {
  @ViewChild(Nav) nav: Nav;
  username: string = "";
  count: Number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.username = this.navParams.get('username');
    this.count = this.navParams.get('views_count');
  }
  nextPage() {
    this.navCtrl.setRoot(ProfileInstructor);
  }
  closePage(){
    this.navCtrl.pop();
  }
}
