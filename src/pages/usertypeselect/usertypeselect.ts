import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { StudentStartPage } from '../studentstart/studentstart';
import { AutorizationsPage } from '../autorizations/autorizations'

@Component({
  selector: 'page-usertypeselect',
  templateUrl: 'usertypeselect.html'
})
/** Окно выбора типа пользователя иструктор или ученик */
export class UserTypeSelectPage {
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen) {
    this.splashScreen.hide();
  }

  /**
   * Открывает нужную страницу в зависимости от выбранноо типа пользователя, либо страницу авторизации инснтруктора либо страницу дальнейшего выбора для ученика
   */
  openPage(page) {
      if(page == 'studentStartPage') {
        this.navCtrl.push(StudentStartPage);
      }
      if(page == 'authorizationPage'){
        this.navCtrl.push(AutorizationsPage, {"typeuser": "instructor"})
      }
  }
}
