import {Component, ViewChild, Renderer2} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import {HttpService} from "../../app/httpService";
import { CheckPhone } from '../checkPhone/checkPhone';
import { FileTransfer } from '@ionic-native/file-transfer';
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import {QuestionWindow} from "../../app/questionWindow";
import { Car } from '../../modules/addAuto/addAuto';
import { Platform } from 'ionic-angular';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-registrationinstructor3',
  templateUrl: 'registrationinstructor3.html',
})
export class RegistrationInstructor3Page {
  @ViewChild(Nav) nav: Nav;
  cars: Car[] = [new Car()];
  defaultPersonImageSrc: string = "assets/imgs/fotik.svg";
  unregisterBackButtonAction: any;
  // Дикий костыль чтобы избавится от "мерцания" при переходе к редактированию первого авто. Скрывает заголовок
  // чтобы сразу открывался заголовок окна редактора машины. Так же делает упрощенный вариант возврата к 2 окну
  // регистрации. Так же показывается только один редактор машины без списка (хотя возможно это избыточно)
  firstCarEdit: boolean = true;

  constructor(public navCtrl: NavController, private http: HttpService, private transfer: FileTransfer,
    public navParams: NavParams, private globalProvider: GlobalProvider, private questionWindow: QuestionWindow,
    private validateWindow: ValidationWindows, private platform: Platform, private renderer2: Renderer2) {
      this.cars[0].photo = 'assets/imgs/tachka.svg';
      this.cars[0].immideatelyEdit = true;
      this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
        this.closePage();
      }, 1);
  }
  delete(index){
    if(this.firstCarEdit) {
      this.unregisterBackButtonAction();
      this.navCtrl.pop();
      return;
    }
    // Если машина не валидная сразу ее удалим. Значит мы отменили редактирование
    if(!this.isValid(this.cars[index])) {
      this.cars.splice(index,1);
      if(this.cars.length == 0)
        this.closePage();
      else {
        // Вернем их обратно
        let buttons = document.getElementsByClassName('regInstr3Buttons');
        this.renderer2.removeClass(buttons[0], 'showHide');
      }
    } else {
      this.questionWindow.showQuestion('Вы действительно хотите удалить машину?', function(){
        this.cars.splice(index,1);
      }, function(){}, this);
    }
  }
  isValid(car) {
    return car.manufacturer && car.gearbox && car.vehicle_type && car.name;
  }
  addAuto(){
    let c = new Car();
    c.photo = 'assets/imgs/tachka.svg';
    c.immideatelyEdit = true;
    this.cars.push(c);
    //Скороем кнопки иначе на иосе они остануться видны
    let buttons = document.getElementsByClassName('regInstr3Buttons');
    this.renderer2.addClass(buttons[0], 'showHide');
  }
  addAutoValue(value, i){
    this.cars[i] = value;
    this.cars[i].id = null;
    this.firstCarEdit = false;
    // Вернем их обратно
    let buttons = document.getElementsByClassName('regInstr3Buttons');
    this.renderer2.removeClass(buttons[0], 'showHide');
  }
  openPage(page) {
    if (page != 'next')
      return;
    if(this.cars.length == 0) {
      this.validateWindow.showValidate('Необходимо добавить хотябы одну машину.');
      return;
    }
    let data = this.navParams.get('data');
    let saveCars = [];
    for(let car of this.cars)
      saveCars.push(car.toJson());
    let body = {
      "password": data.registration1.password,
      'phone': data.registration1.tel,
      "fio": data.registration1.name,
      "birthyear": data.registration2.birthYear,
      'sex': data.registration2.sex,
      'city':data.registration2.city,
      "comment": '',
      "startworkyear": data.registration2.startYear,
      'cars': saveCars
    };

    let loading = this.globalProvider.createLoadingController();
    loading.present();

    this.http.postData('/instructor/create', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let body = JSON.parse(res.data);
        if(data.fileUri != this.defaultPersonImageSrc) {
          let fileTransfer = this.transfer.create();
          let options = {
              fileKey: 'photo',
              params: {
                  'instructorId': body.instructorid
              }
          };
          fileTransfer.upload(data.fileUri, this.http.urlServer+'/instructor/uploadinstructorregistrationphoto', options, true)
            .then((data) => {
              console.log(data);
            }, (err) => {
              console.log(err);
            })
        }
        this.uploadCarPhotos(body);
        this.checkPhone();
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  uploadCarPhotos(body) {
    for(let i = 0; i < this.cars.length; i++) {
      if(this.cars[i].photo != 'assets/imgs/tachka.svg') {
        let fileTransfer = this.transfer.create();
        let options = {
          fileKey: 'photo',
          params: {
              'carId': body.carsid[i]
          }
        };
        fileTransfer.upload(this.cars[i].photo, this.http.urlServer+'/instructor/uploadcarregistrationphoto', options, true)
          .then((data) => {
            console.log(data);
          }, (err) => {
            console.log(err);
          })
        }
    }
  }
  checkPhone(){
    this.unregisterBackButtonAction();
    this.navCtrl.setRoot(CheckPhone, {
      "data": {
        "tel": this.navParams.get("data").registration1.tel,
        "type": "instructor"
      }
    });
  }
  closePage(){
    this.unregisterBackButtonAction();
    if(this.cars.length != 0) {
      this.questionWindow.showQuestion('Введенные данные не сохранятся. Вернуться назад?', function(){
        this.navCtrl.pop();
      }, function(){}, this);
    } else
      this.navCtrl.pop();
  }
}
