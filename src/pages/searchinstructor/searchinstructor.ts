import {Component} from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import {ResultSearchPage} from '../resultsearch/resultseartch'
import {HttpService} from "../../app/httpService";
import { ValidationWindows } from '../../app/validationWindows';
import { GlobalProvider } from '../../app/globalSettingsProvider';
import {UserTypeSelectPage} from "../usertypeselect/usertypeselect";
import {QuestionWindow} from '../../app/questionWindow';
import {Cookie} from "ng2-cookies";
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-searchinstructor',
  templateUrl: 'searchinstructor.html',
})
export class SearchinstructorPage {
  manufacturedValue: any = null;
  gearBoxValue: any = null;
  sexValue: any = null;
  vehicleTypeValue: any = null;
  cityValue: any = null;
  categories: string = JSON.stringify([{'title':'A','id':'A'},{'title':'B','id':'B'},{'title':'C','id':'C'},{'title':'D','id':'D'},{'title':'E','id':'E'}]);
  gearboxes: string = JSON.stringify([{'title':'Механическая','id':'mechanic'},{'title':'Автоматическая','id':'automatic'}]);
  manufacturers: string = JSON.stringify([{'title':'Отечественная','id':'russian'},{'title':'Иномарка','id':'foreign'}]);
  sexs: string = JSON.stringify([{'title':'Мужской','id':'male'},{'title':'Женский','id':'female'}]);
  cities: string = JSON.stringify([]);
  cityAlias: string = '';
  logoutsrc: string = '';
  backbuttonvisibility: string = 'true';
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpService,
    private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder, private validateWindow: ValidationWindows,
    private globalProvider: GlobalProvider, private questionWindow: QuestionWindow) {
    if(!this.navParams.get('back'))
      this.backbuttonvisibility = 'false';
    if(localStorage.getItem('Session') == 'student')
      this.logoutsrc = '';
    this.loadCities();
  }
  closePage(){
    this.navCtrl.pop();
  }
  onChangedCity(selectedValue: any) {
    this.cityValue = selectedValue;
  }
  onChangedManufacturer(selectedValue: any) {
    this.manufacturedValue = selectedValue;
  }
  onChangedGearbox(selectedValue: any) {
    this.gearBoxValue = selectedValue;
  }
  onChangedCategory(selectedValue: any) {
    this.vehicleTypeValue = selectedValue;
  }
  onChangedSex(selectedValue: any) {
    this.sexValue = selectedValue;
  }
  openPage(page){
    if(page=="search"){
      let body = {
        "page": 1,
        "per_page": 50
      };
      if(this.manufacturedValue != null)
        body['manufacturer'] = this.manufacturedValue;
      if(this.gearBoxValue != null)
        body['gearbox'] = this.gearBoxValue;
      if(this.vehicleTypeValue != null)
        body['vehicle_type'] = this.vehicleTypeValue;
      if(this.sexValue != null)
        body['sex'] = this.sexValue;
      if(this.cityValue != null)
        body['city'] = this.cityValue;
        
      let errFields = [];
      if(!this.cityValue)
        errFields.push('"Город"');
      if(!this.vehicleTypeValue)
        errFields.push('"Категория"');
      if(!this.gearBoxValue)
        errFields.push('"КПП"');
      if(errFields.length > 0) {
        let text = errFields.length > 1 ? "Необходимо заполнить поля " : "Необходимо заполнить поле ";
        text += errFields.join(', ');
        this.validateWindow.showValidate(text);
        return;
      }
      
      let loading = this.globalProvider.createLoadingController();
      loading.present();
      
      this.http.postData('/instructor/search', body)
        .then((res: HTTPResponse) => {
          loading.dismiss();
          let data = JSON.parse(res.data);
          if(data.total > 0)
            this.navCtrl.push(ResultSearchPage, {"data":data, 'body': body});
          else
            this.validateWindow.showValidate('Инструкторы не найдены. Попробуйте изменить параметры поиска.');
        })
        .catch((err: HTTPResponse) => {
          loading.dismiss();
          let errcode = err.status;
          switch(errcode) {
            case 0:
              this.validateWindow.showConnectionLostMessage();
              break;
            case 500:
              this.validateWindow.showServerErrorMessage();
              break;
            default:
              this.validateWindow.showUnknownErrorMessage();
          }
          console.log("error");
          console.log(err.error);
        });
    }
  }
  loadCities() {
    this.http.getData('/region/city')
      .then((res: HTTPResponse) => {
        let body = JSON.parse(res.data);
        let data = [];
        for(let city of body) {
          data.push({
            'title': city.name,
            'id': city.id
          })
        }
        this.cities = JSON.stringify(data);
        this.findGeolocation();
      })
      .catch((err: HTTPResponse) => {
        console.log("error");
        console.log(err.error);
      });
  }
  findGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      //57.867766, 39.531339 - тутаев
      //59.874746, 30.390485 - питер
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          let locality = result[0].locality;
          let cities = JSON.parse(this.cities);
          for(let i = 0; i < cities.length; i++) {
            if(cities[i].title == locality) {
              this.cityValue = cities[i].id;
              this.cityAlias = cities[i].title;
              break;
            }
          }
        })
        .catch((error: any) => console.log(error));
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  logout(){
    this.questionWindow.showQuestion('Вы действительно хотите выйти?', function(){
      /*this.http.postData('/account/clearfcmtoken', {})
        .then((res: HTTPResponse) => {*/
          this.http.getData('/logout')
            .then((res: HTTPResponse) => {
              localStorage.removeItem('Session');
              Cookie.delete('carinstructorfinder.sid');
              this.navCtrl.setRoot(UserTypeSelectPage);
            });
        //});
      }, function(){}, this);
  }
}
