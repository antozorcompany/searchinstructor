import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { CallNumber } from '@ionic-native/call-number';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { HttpService } from '../../app/httpService'
import { CarEditView } from '../carEditView/carEditView'
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { Comments } from '../comments/comments';
import { CommentWindows } from '../../app/commentWindow';
import { Car } from '../../modules/addAuto/addAuto';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-profileinstructorforstudent',
  templateUrl: 'profileInstructorForStudent.html',
  providers: [HttpService]
})
export class ProfileInstructorForStudent {
  birthYear: number;
  fio: string = '';
  phone: string = '';
  phoneToCall: string = '';
  likes: number;
  dislikes: number;
  reviews: number;
  startYear: number;
  sex:string;
  city:string;
  cars: Car[] = [];
  instructorId: number;
  reviewsFormat: string;
  photoSrc: string = 'assets/imgs/chel.svg';
  callback: any;
  profileLoaded: boolean = false;
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen, public navParams: NavParams,
    private callNumber: CallNumber, private http: HttpService, private globalProvider: GlobalProvider,
    private commentWindow: CommentWindows, private validateWindow: ValidationWindows,
    private androidPermissions: AndroidPermissions) {
    this.splashScreen.hide();
    this.instructorId = this.navParams.get("instructorId");
    this.callback = this.navParams.get("callback");
  }
  ionViewWillEnter(){
    this.loadProfile();
  }
  reqFunc(like) {
    let body = {
      instructorId: this.instructorId
    };
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    let req = like ? '/vote/like' : '/vote/dislike';
    this.http.postData(req, body)
      .then((res: HTTPResponse)=>{
        loading.dismiss();
        this.loadProfile();
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        if(errcode == 400) {
          let resbody = err.error;
          if(resbody == 'need review') {
            if(like)
              this.commentWindow.showComment(this.showCommentForLike, null, this);
            else
              this.commentWindow.showComment(this.showCommentForDislike, null, this);
          }
          else
            this.processError(err);
        }
        else
          this.processError(err);
      });
  }
  likeClick(){
    this.reqFunc(true);
  }
  showCommentForLike() {
    this.navCtrl.push(Comments, {id: this.instructorId, type: 'mark', like: true});
  }
  dislikeClick(){
    this.reqFunc(false);
  }
  showCommentForDislike() {
    this.navCtrl.push(Comments, {id: this.instructorId, type: 'mark', like: false});
  }
  call(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
      .then((res) => {
        if(res.hasPermission)
          this.makeCall()
        else
          this.requestPermission()
      })
      .catch(() => {
        this.requestPermission()
      });
  }
  requestPermission() {
    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
      .then((res) => {
        if(res.hasPermission)
          this.makeCall()
      });
  }
  makeCall() {
    this.callNumber.callNumber(this.phoneToCall, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }
  closePage(){
    this.callback(this.instructorId, this.likes, this.dislikes);
    this.navCtrl.pop();
  }
  viewAuto(){
    this.navCtrl.push(CarEditView, {'cars': this.cars, 'type': 'view'})
  }
  stringPhone(numberPhone){
    let stringPhone = '';
    for(let i = 0; i < numberPhone.length; i++){
      if(i == 0){
        stringPhone += '+';
      }
      if(i==1){
        stringPhone+= '(';
      }
      if(i==4){
        stringPhone+= ') ';
      }
      if(i == 7){
        stringPhone+='-'
      }
      stringPhone+=numberPhone[i];
    }
    return stringPhone;
  }
  loadProfile() {
    let loading = this.globalProvider.createLoadingController();
    loading.present();

    this.http.postData('/instructor/profile', {'instructorId':this.instructorId})
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let data = JSON.parse(res.data);
        this.birthYear = data.birthyear;
        this.startYear = data.startworkyear;
        this.phoneToCall = '+' + data.phone;
        this.phone = this.stringPhone(data.phone);
        this.fio = data.fio;
        this.sex = data.sex == 'male'?'Мужской':'Женский';
        this.city = data.cityname;
        this.likes = data.likes;
        this.dislikes = data.dislikes;
        this.reviews = data.reviews;
        this.reviewsFormat = this.globalProvider.getReviewFormat(this.reviews);
        this.cars = [];
        for(let c of data.cars){
          let car = new Car(c.id, c.name, c.vehicle_type, c.gearbox, c.manufacturer, c.photo_url);
          if(car.photo)
            car.photo = this.http.urlServer + car.photo + '?t=' + (new Date()).getTime();
          else
            car.photo = 'assets/imgs/tachka.svg';
          this.cars.push(car);
        }
        if(data.photo_md5)
            this.photoSrc = this.http.urlServer + data.photo_url + '?t=' + (new Date()).getTime();
        this.profileLoaded = true;
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  viewReviews(){
    this.navCtrl.push(Comments, {id:this.instructorId})
  }
  processError(err) {
    let errcode = err.status;
    switch(errcode) {
      case 0:
        this.validateWindow.showConnectionLostMessage();
        break;
      case 500:
        this.validateWindow.showServerErrorMessage();
        break;
      case 400:
        this.validateWindow.showValidate('Вы ранее уже поставили оценку этому инструктору.');
        break;
      case 401:
        this.validateWindow.showValidate('Необходимо войти чтобы поставить оценку.');
        break;
      default:
        this.validateWindow.showUnknownErrorMessage();
    }
  }
}
