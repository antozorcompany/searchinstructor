import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';
import { NavController } from 'ionic-angular';

import { AutorizationsPage } from '../autorizations/autorizations';
import { SearchinstructorPage} from '../searchinstructor/searchinstructor';


@Component({
  selector: 'page-studentstartpage',
  templateUrl: 'studentstart.html'
})
/** Страница для выбора учеником дальнейших действий */
export class StudentStartPage {
  @ViewChild(Nav) nav: Nav;
  constructor(public navCtrl: NavController) {
  }
  
  /**
   * Открывает нужную страницу в зависимости от выбора пользователя, либо страницу поиска либо страницу авторизации для ученика
   */
  openPage(page) {
    if (page == 'searchInstructor')
      this.navCtrl.push(SearchinstructorPage, {'back': true});
    if(page == 'autorizationsPage')
      this.navCtrl.push(AutorizationsPage, {"typeuser": "student"});
  }
  closePage(){
    this.navCtrl.pop();
  }
}
