import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { SearchinstructorPage } from '../searchinstructor/searchinstructor'
import {FCMService} from "../../app/fcmService";

@Component({
  selector: 'page-endregistrationstudent',
  templateUrl: 'endRegistrationStudent.html'
})
export class EndRegistrationStudent {
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen, private fcm: FCMService) {
    this.splashScreen.hide();
  }

  openPage(page) {
    if(page == 'profile') {
      this.fcm.initNotifications();
      this.navCtrl.setRoot(SearchinstructorPage, {'back': false});
    }
  }
}
