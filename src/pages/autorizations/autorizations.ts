import {Component, ViewChild, Renderer, ElementRef} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import { HTTPResponse } from '@ionic-native/http';

import {RegistrationstudentPage} from "../registrationStudent/registrationstudent";
import { RegistratIoninstructor1Page } from '../registrationInstructor1/registrationinstructor1'
import {Keyboard} from "@ionic-native/keyboard";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpService } from '../../app/httpService'
import { StudentAuthPage } from '../studentauth/studentauth'
import { InstructorAuthPage } from '../instructorauth/instructorauth'
import {RequestChangePassword}  from '../requestChangePassword/requestChangePassword'
import { NeedUpdatePage } from '../needupdate/needupdate';
import {FCMService} from "../../app/fcmService";
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { CheckPhone } from '../checkPhone/checkPhone';

@Component({
  selector: 'page-autorizations',
  templateUrl: 'autorizations.html',
  providers: [HttpService]
})

export class AutorizationsPage {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('passwordItem') input1: ElementRef;
  
  typeuser : String;
  public mask = ['+','7','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  private AutorizationForm: FormGroup;
  private showBackButton:string = 'true';
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
    public keyboard: Keyboard, private http: HttpService, private fcm: FCMService,
    private globalProvider: GlobalProvider, private validateWindow: ValidationWindows,
    private renderer: Renderer) {
    this.AutorizationForm = this.formBuilder.group({
      tel: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.typeuser = this.navParams.get('typeuser');
    this.showBackButton = this.navParams.get('backButton') == false ? 'false' : 'true';
  }
  focusNext(number){
    switch(number) {
      case 1:
        this.renderer.invokeElementMethod(this.input1.nativeElement, 'focus');
        break;
      case 2:
        this.keyboard.hide();
        break;
    }
  }
  Autorization() {
    let tel = this.AutorizationForm.value.tel;
    tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    let body = {
      "v": 1,
      "phone": tel,
      "password": this.AutorizationForm.value.password
    };
    console.log(body);
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/login',body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        this.succes(res);
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          case 401:
            this.validateWindow.showValidate('Не верный номер телефона или пароль');
            break;
          case 400:
            let text = err.error;
            if(text == "wrong version") {
              this.navCtrl.setRoot(NeedUpdatePage);
              break;
            }
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err);
      });
  }
  focusMask(phone){
      phone.placeholder = "+7(___) ___-____";
  }
  openPage(page) {
    console.log(page)
    if (page == 'registration') {
      if (this.typeuser == 'student') {
        this.navCtrl.push(RegistrationstudentPage);
      } else {
        if (this.typeuser == 'instructor')
          this.navCtrl.push(RegistratIoninstructor1Page);
      }
    } else {
        this.navCtrl.push(RequestChangePassword);
    }
  }
  succes(Res: HTTPResponse){
    let res = JSON.parse(Res.data)
    if(res.status == 'ok'){
      this.fcm.initNotifications();
      if(res.typeSession == 'instructor'){
        this.navCtrl.setRoot(InstructorAuthPage, {
            'username': res.username,
            'views_count': res.views_count
        });
        localStorage.setItem("Session", "instructor");
      } else if(res.typeSession == 'student') {
        this.navCtrl.setRoot(StudentAuthPage, {
          'username': res.username
        });
        localStorage.setItem("Session", "student");
      }
    } else if(res.status == 'unconfirmed') {
      this.validateWindow.showValidate('Для введенного номера телефона есть неподтвержденная учетная запись. Необходимо подтверждение.', function() {
        let tel = this.AutorizationForm.value.tel;
        tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
        this.http.postData('/account/resendcode', {'phone': tel})
          .then((result: HTTPResponse) => {
            this.navCtrl.setRoot(CheckPhone, {
              "data": {
                "tel": tel,
                "type": res.typeSession
              }
            });
          })
      }, this);
    }
  }
  error(response){
    console.log(response);
  }
  closePage(){
    this.navCtrl.pop();
  }
  rightButton(){
    console.log('hehe right button');
  }
}
