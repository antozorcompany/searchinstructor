import {Component, SimpleChanges} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { Comment } from '../comment/comment'
import { HttpService } from '../../app/httpService'
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'comments',
  templateUrl: 'comments.html'
})
export class Comments {
  instructorId: number;
  reviewArray: any[] = [];
  checkReview: boolean = false;
  writecommentsrc: string = '';
  checkFocus: boolean = false;
  markComment: boolean = false;
  like: boolean = false;
  loading: any;
  page: number = 1;
  pages: number = 1;
  constructor(private navCtrl: NavController, private navParams: NavParams, private http: HttpService, private globalProvider: GlobalProvider, private validateWindow: ValidationWindows) {
    this.instructorId = this.navParams.get('id');
    if(this.navParams.get('type') == 'mark') {
      this.markComment = true;
      this.checkReview = true;
      this.like = this.navParams.get('like');
    }
    if(localStorage.getItem('Session') != 'instructor')
      this.writecommentsrc = '';
    this.loadReview(1);
  }

  ngOnChanges(changes: SimpleChanges) {
  }
  loadReview(page){
    let data = {
      instructorId: this.instructorId,
      page: page,
      per_page: 10
    };
    this.http.postData('/review/get', data)
      .then((res: HTTPResponse) => {
        let body = JSON.parse(res.data);
        this.pages = body.pages;
        if(page != 1) {
          for(let comment of body.data) {
            if(this.reviewArray[this.reviewArray.length-1].id > comment.id)
              this.reviewArray.push(comment);
          }
        } else {
          let data = body.data.reverse();
          for(let comment of data) {
            if(this.reviewArray.length == 0) {
              this.reviewArray.unshift(comment);
            } else {
              if(this.reviewArray[0].id < comment.id)
                this.reviewArray.unshift(comment);
            }
          }  
        }
      }).
      catch((err: HTTPResponse) => {
        console.log('error', err.error)
      })
  }
  scroll = (): void => {
    let scrollBlocks = document.getElementById('comments-item');
    if(scrollBlocks.scrollHeight- window.innerHeight - scrollBlocks.scrollTop < 100){
      if(this.page < this.pages){
        this.page += 1;
        this.loadReview(this.page)
      }
    }
  };
  selected(reviewItem) {
    this.navCtrl.push(Comment, {review: reviewItem.review, student: reviewItem.student});
  }
  closePage(){
    this.navCtrl.pop();
  }
  sendReview(reviewText){
    this.loading = this.globalProvider.createLoadingController();
    this.loading.present();
    let data = {
      instructorId: this.instructorId,
      review: reviewText.innerText
    };
    this.http.postData('/review/create', data)
      .then((res: HTTPResponse) => {
        this.loadReview(1);
        reviewText.innerText = "";
        this.checkReview = false;
        if(this.markComment)
          this.makeVote();
        else
          this.loading.dismiss();
      })
      .catch((err: HTTPResponse)=>{
        console.log('error', err.error);
        this.loading.dismiss();
      })
  }
  makeVote() {
    let body = {
      instructorId: this.instructorId
    };
    let req = '';
    if(this.like)
      req = '/vote/like';
    else
      req = '/vote/dislike';
    this.http.postData(req, body)
      .then((res: HTTPResponse) => {
        this.loading.dismiss();
      })
      .catch((err: HTTPResponse) => {
        this.loading.dismiss();
      });
  }
  rightButton(){
    if(localStorage.getItem('Session') != 'student') {
      this.validateWindow.showValidate('Необходимо войти чтобы оставить отзыв.');
      return;
    }
    this.checkReview = !this.checkReview;
  }
  focusInput(input){
    input.focus();
  }
  valuechange(input){
    if(input.target.innerText == '') {
      this.checkFocus = false;
    }
    else {
      this.checkFocus = true;
    }
  }
}
