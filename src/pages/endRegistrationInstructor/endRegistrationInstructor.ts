import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import { ProfileInstructor } from '../profileInstructor/profileInstructor'
import {FCMService} from "../../app/fcmService";

@Component({
  selector: 'page-endregistrationinstructor',
  templateUrl: 'endRegistrationInstructor.html'
})
export class EndRegistrationInstructor {
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen, private fcm: FCMService) {
    this.splashScreen.hide();
  }

  openPage(page) {
    if(page == 'profile') {
      this.fcm.initNotifications();
      this.navCtrl.setRoot(ProfileInstructor);
    }
  }
}
