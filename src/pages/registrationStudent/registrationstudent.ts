import {Component, ViewChild, Injectable, ElementRef, Renderer} from '@angular/core';

import { Nav, NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Keyboard } from "@ionic-native/keyboard";
import 'rxjs/Rx';
import { HTTPResponse } from '@ionic-native/http';

import {HttpService} from "../../app/httpService";
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { CheckPhone } from '../checkPhone/checkPhone';
import { ValidationWindows } from '../../app/validationWindows';

@Component({
  selector: 'page-registationstudent',
  templateUrl: 'registrationstudent.html',
})

@Injectable()
export class RegistrationstudentPage {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('tel') input1: ElementRef;
  @ViewChild('passwordItem') input2: ElementRef;
  @ViewChild('confirmItem') input3: ElementRef;

  private RegisterStudent: FormGroup;
  public mask = ['+','7','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  constructor(private renderer: Renderer,public navCtrl: NavController, private formBuilder: FormBuilder, public keyboard: Keyboard,
    private http: HttpService, private globalProvider: GlobalProvider, private validateWindow: ValidationWindows) {
    this.RegisterStudent = this.formBuilder.group({
      tel: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  focusNext(number){
    console.log(number);
    switch(number) {
      case 1:
        this.renderer.invokeElementMethod(this.input1.nativeElement, 'focus');
        break;
      case 2:
        this.renderer.invokeElementMethod(this.input2.nativeElement, 'focus');
        break;
      case 3:
        if(this.checkPassword())
          this.renderer.invokeElementMethod(this.input3.nativeElement, 'focus');
        break;
      case 4:
        this.keyboard.hide();
        break;
    }
  }
  hasNumber(str) {
    return /\d/.test(str);
  }
  hasLetter(str) {
    return /[a-zA-Z]/.test(str);
  }
  checkPassword(){
    let password = this.RegisterStudent.value['password'];
    if(password.length < 6 || !this.hasNumber(password) || !this.hasLetter(password)) {
      this.validateWindow.showValidate('Пароль должен состоять из цифр, букв и состоять не менее чем из 6 символов.');
      return false;
    }
    return true;
  }
  focusMask(phone){
      phone.placeholder = "+7(___) ___-____";
  }
  logForm(){
    console.log(this.RegisterStudent.value['tel']);
  }
  Registration() {
    if(!this.checkPassword())
      return;
    if (this.RegisterStudent.value.password != this.RegisterStudent.value.confirmPassword) {
      this.validateWindow.showValidate('Пароль и подтверждение не совпадают.');
      return;
    }
    let tel = this.RegisterStudent.value['tel'];
    tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/checkphone', {
      "phone": tel
    })
    .then((Res: HTTPResponse)=>{
      let res = JSON.parse(Res.data)
      if(res.status == 'ok'){
        let body = {
          phone: tel,
          fio: this.RegisterStudent.value['name'],
          password: this.RegisterStudent.value['password']
        };
        this.http.postData('/student/create', body)
          .then((response: HTTPResponse) => {
            loading.dismiss();
            this.navCtrl.setRoot(CheckPhone, {
              "data": {
                "tel": tel,
                "type": "student"
              }
            });
          })
          .catch((err: HTTPResponse) => {
            loading.dismiss();
            this.showError(err);
            console.log(err.error);
          });
      }
      else if(res.status == 'unconfirmed') {
        loading.dismiss();
        this.validateWindow.showValidate('Для введенного номера телефона есть неподтвержденная учетная запись. Необходимо подтверждение.', function() {
          this.http.postData('/account/resendcode', {'phone': tel})
            .then((res: HTTPResponse) => {
              this.navCtrl.setRoot(CheckPhone, {
                "data": {
                  "tel": tel,
                  "type": "student"
                }
              });
            })
        }, this);
      } else {
        loading.dismiss();
        this.validateWindow.showValidate('Для введенного номера телефона уже есть учетная запись.');
      }
    })
    .catch(err=>{
      loading.dismiss();
      this.showError(err);
      console.log("error");
      console.log(err);
    });
  }
  showError(err) {
    let errcode = err.status;
    switch(errcode) {
      case 0:
        this.validateWindow.showConnectionLostMessage();
        break;
      case 500:
        this.validateWindow.showServerErrorMessage();
        break;
      default:
        this.validateWindow.showUnknownErrorMessage();
    }
  }
  closePage(){
    this.navCtrl.pop();
  }
}
