import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';


@Component({
  selector: 'comment',
  templateUrl: 'comment.html'
})
export class Comment {
  review: string;
  student: string;

  constructor(private navCtrl: NavController, private navParams: NavParams) {
    this.student = this.navParams.get('student');
    this.review = this.navParams.get('review');
  }
  closePage(){
    this.navCtrl.pop();
  }


}
