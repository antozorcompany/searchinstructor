import { Component } from '@angular/core';
import {NavController, NavParams, normalizeURL} from 'ionic-angular';
import { SplashScreen} from '@ionic-native/splash-screen';
import {Cookie} from "ng2-cookies";
import {UserTypeSelectPage} from "../usertypeselect/usertypeselect";
import { HttpService } from '../../app/httpService'
import {CarEditView} from "../carEditView/carEditView";
import { GlobalProvider } from '../../app/globalSettingsProvider';
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import { ProfileInstructor} from '../profileInstructor/profileInstructor';
import {QuestionWindow} from '../../app/questionWindow';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import {PhotoSelectorWindow} from "../../app/photoSelectorWindow";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser'
import { FileTransfer } from '@ionic-native/file-transfer';
import { Car } from '../../modules/addAuto/addAuto';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-editprofileinstructor',
  templateUrl: 'editProfileInstructor.html'
})
export class EditProfileInstructor {
  birthday: any[] = [];
  start_teaching: any[] = [];
  birthdayString: string;
  start_teachingString: string;
  fio: string;
  phone: string;
  private RegisterInstructor: FormGroup;
  public mask = ['+','7','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  cars: Car[] = [];
  cities: string = JSON.stringify([]);
  cityAlias: string = '';
  startYearAlias: string = '';
  birthYearAlias: string = '';
  sexs: string = JSON.stringify([{'title':'Мужской','id':'male'},{'title':'Женский','id':'female'}]);
  sexString: string = '';
  profileData: any;
  defaultImageSrc: string = "assets/imgs/fotik.svg";
  imageSrc: SafeUrl = this.defaultImageSrc;
  fileUri:string = this.defaultImageSrc;
  
  constructor(public navCtrl: NavController, public splashScreen: SplashScreen, public navParams: NavParams,
    private http: HttpService, private globalProvider: GlobalProvider, private formBuilder: FormBuilder,
    private camera: Camera, private photoSelectorWindow: PhotoSelectorWindow, private ds: DomSanitizer,
    private transfer: FileTransfer, private questionWindow: QuestionWindow, private crop: Crop) {
    this.RegisterInstructor = this.formBuilder.group({
      tel: ['', Validators.required],
      name: ['', Validators.required]
    });
    this.splashScreen.hide();
    this.profileData = Object.assign({}, this.navParams.get("res"));
    let year = new Date().getFullYear();
    for (let i = year-18-3; i >= (year-60); i--){
      this.birthday.push({'title': i, 'id': i});
    }
    for (let i = year-3; i >= (year-42); i--){
      this.start_teaching.push({'title': i, 'id': i});
    }
    this.start_teachingString = JSON.stringify(this.start_teaching);
    this.birthdayString = JSON.stringify(this.birthday);

    this.birthYearAlias = this.profileData.birthyear + ' г.р.';
    this.startYearAlias = 'c ' + this.profileData.startworkyear + ' г.';
    this.phone = this.stringPhone(this.profileData.phone);
    this.fio = this.profileData.fio;
    this.cityAlias = this.profileData.cityname;
    this.sexString = this.profileData.sex == 'male'?'Мужской':'Женский';
    for(let c of this.profileData.cars) {
      let car = new Car(c.id, c.name, c.vehicle_type, c.gearbox, c.manufacturer, c.photo_url);
      if(car.photo)
          car.photo = this.http.urlServer + car.photo + '?t=' + (new Date()).getTime();
        else
          car.photo = 'assets/imgs/tachka.svg';
      this.cars.push(car);
    }
    if(this.profileData.photo_md5)
        this.imageSrc = this.http.urlServer + this.profileData.photo_url + '?t=' + (new Date()).getTime();
    this.loadCities();
  }

  focusMask(phone){
    phone.placeholder = "+7(___) ___-____";
  }
  stringPhone(numberPhone){
    let stringPhone = '';
    for(let i = 0; i < numberPhone.length; i++){
      if(i == 0){
        stringPhone += '+';
      }
      if(i==1){
        stringPhone+= '(';
      }
      if(i==4){
        stringPhone+= ') ';
      }
      if(i == 7){
        stringPhone+='-'
      }
      stringPhone+=numberPhone[i];
    }
    return stringPhone;
  }
  closePage(){
    this.navCtrl.pop();
  }
  saveProfile(){
    let tel = this.RegisterInstructor.value.tel;
    let tel1 = this.phone;
    tel = tel.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    tel1 = tel1.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('+', '');
    let saveCars = [];
    for(let car of this.cars)
      saveCars.push(car.toJson());
    let body = {
      'phone': tel || tel1,
      "fio": this.RegisterInstructor.value.name || this.fio,
      "birthyear": this.profileData.birthyear,
      'sex': this.profileData.sex,
      'city': this.profileData.city,
      "startworkyear": this.profileData.startworkyear,
      'cars': saveCars
    };

    let loading = this.globalProvider.createLoadingController();
    loading.present();

    this.http.postData('/instructor/edit', body)
      .then((res: HTTPResponse) => {
        let resBody = JSON.parse(res.data);
        this.uploadCarPhotos(resBody);
        if(this.fileUri != this.defaultImageSrc) {
          let fileTransfer = this.transfer.create();
          let options = {
              fileKey: 'photo',
              params: {
                  'instructorId': this.profileData.id
              }
          };
          fileTransfer.upload(this.fileUri, this.http.urlServer+'/instructor/uploadinstructorregistrationphoto', options, true)
            .then((data) => {
              console.log(data);
              loading.dismiss();
              this.navCtrl.setRoot(ProfileInstructor);
            }, (err) => {
              console.log(err);
              loading.dismiss();
              this.navCtrl.setRoot(ProfileInstructor);
            })
        }
        else {
          loading.dismiss();
          this.navCtrl.setRoot(ProfileInstructor);
        }
      })
  }
  uploadCarPhotos(body) {
    for(let i = 0; i < this.cars.length; i++) {
      if(this.cars[i].photo != 'assets/imgs/tachka.svg' && !this.cars[i].photo.includes('http')) {
        let fileTransfer = this.transfer.create();
        let options = {
          fileKey: 'photo',
          params: {
              'carId': body.carsid[i]
          }
        };
        fileTransfer.upload(this.cars[i].photo, this.http.urlServer+'/instructor/uploadcarregistrationphoto', options, true)
          .then((data) => {
            console.log(data);
          }, (err) => {
            console.log(err);
          })
        }
    }
  }
  logout(){
    this.questionWindow.showQuestion('Вы действительно хотите выйти?', function(){
      /*this.http.postData('/account/clearfcmtoken', {})
        .then((res: HTTPResponse) => {*/
          this.http.getData('/logout')
            .then((res: HTTPResponse) => {
              localStorage.removeItem('Session');
              Cookie.delete('carinstructorfinder.sid');
              this.navCtrl.setRoot(UserTypeSelectPage);
            });
        //});
      }, function(){}, this);
  }
  loadCities() {
    this.http.getData('/region/city')
      .then((res: HTTPResponse) => {
        let body = JSON.parse(res.data);
        let data = [];
        for(let city of body) {
          data.push({
            'title': city.name,
            'id': city.id
          })
        }
        this.cities = JSON.stringify(data);
      })
      .catch((err: HTTPResponse) => {
        console.log("error");
        console.log(err.error);
      });
  }
  viewAuto(){
    this.navCtrl.push(CarEditView, {
      'cars': this.cars,
      'type': 'edit',
      'callback': this.setCarEdit,
    });
  }
  setCarEdit = (cars) => {
    this.cars = cars;
  };
  onSelectChangeBirth(selectedValue: any) {
    this.profileData.birthyear = selectedValue;
    this.birthYearAlias = selectedValue + ' г.р.';
  }
  onSelectChangeStart(selectedValue: any) {
    this.profileData.startworkyear = selectedValue;
    this.startYearAlias = 'с ' + selectedValue + ' г.';
  }
  onSelectChangedSex(selectedValue: any){
    this.profileData.sex = selectedValue;
  }
  onSelectChangedCity(selectedValue: any){
    this.profileData.city = selectedValue;
  }
  selectPhotoType() {
    this.photoSelectorWindow.showWindow(this.selectFromGalary, this.makePhoto, this);
  }
  selectFromGalary() {
    this.selectPhoto(true);
  }
  makePhoto() {
    this.selectPhoto(false);
  }
  selectPhoto(galary:boolean) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.FRONT,
      correctOrientation: true
    };
    if(galary)
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.camera.getPicture(options)
      .then((file_uri) => {
        this.crop.crop(file_uri, { quality: 100, targetWidth: -1, targetHeight: -1 })
          .then((path) => {
            path = normalizeURL(path);
            this.imageSrc = this.ds.bypassSecurityTrustUrl(path);
            this.fileUri = path;
          })
      })
  }
}
