import {Component, ViewChild, Renderer, ElementRef} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';
import { HTTPResponse } from '@ionic-native/http';
import {Keyboard} from "@ionic-native/keyboard";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import { HttpService } from '../../app/httpService'
import { GlobalProvider } from '../../app/globalSettingsProvider';
import { ValidationWindows } from '../../app/validationWindows';
import { AutorizationsPage } from '../autorizations/autorizations';

@Component({
  selector: 'page-changePassword',
  templateUrl: 'changePassword.html',
  providers: [HttpService]
})
export class ChangePassword {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('confirmItem') input1: ElementRef;
  @ViewChild('codeItem') input2: ElementRef;
  
  private RegisterInstructor: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public keyboard: Keyboard,
    private formBuilder: FormBuilder, private http: HttpService, private globalProvider: GlobalProvider,
    private validateWindow: ValidationWindows, private renderer: Renderer) {
    this.RegisterInstructor = this.formBuilder.group({
      code: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  submitAgain() {
    let body = {
        'phone': this.navParams.get("data").tel
    }
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/requestchangepassword', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let resbody = res.data;
        if(resbody == 'error')
          this.validateWindow.showValidate('Не найдена учетная запись для введенного номера телефона.');
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  sendCode() {
    if(!this.checkPassword())
      return;
    if (this.RegisterInstructor.value.password != this.RegisterInstructor.value.confirmPassword) {
      this.validateWindow.showValidate('Пароль и подтверждение не совпадают.');
      return;
    }
    let body = {
        'phone': this.navParams.get("data").tel,
        'code': this.RegisterInstructor.value.code,
        'password': this.RegisterInstructor.value.password
    }
    
    let loading = this.globalProvider.createLoadingController();
    loading.present();
    
    this.http.postData('/account/changepassword', body)
      .then((res: HTTPResponse) => {
        loading.dismiss();
        let body = res.data;
        if(body == 'error')
          this.validateWindow.showValidate('Не верно введен код из СМС.');
        else
          this.navCtrl.setRoot(AutorizationsPage, {'backButton':false});
      })
      .catch((err: HTTPResponse) => {
        loading.dismiss();
        let errcode = err.status;
        switch(errcode) {
          case 0:
            this.validateWindow.showConnectionLostMessage();
            break;
          case 500:
            this.validateWindow.showServerErrorMessage();
            break;
          default:
            this.validateWindow.showUnknownErrorMessage();
        }
        console.log("error");
        console.log(err.error);
      });
  }
  focusNext(number){
    switch(number) {
      case 1:
        if(this.checkPassword())
          this.renderer.invokeElementMethod(this.input1.nativeElement, 'focus');
        break;
      case 2:
        this.renderer.invokeElementMethod(this.input2.nativeElement, 'focus');
        break;
      case 3:
        this.keyboard.hide();
        break;
    }
  }
  hasNumber(str) {
    return /\d/.test(str);
  }
  hasLetter(str) {
    return /[a-zA-Z]/.test(str);
  }
  checkPassword(){
    let password = this.RegisterInstructor.value.password;
    if(password.length < 6 || !this.hasNumber(password) || !this.hasLetter(password)) {
      this.validateWindow.showValidate('Пароль должен состоять из цифр, букв и состоять не менее чем из 6 символов.');
      return false;
    }
    return true;
  }
}
