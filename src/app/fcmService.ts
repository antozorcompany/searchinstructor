import {Platform} from 'ionic-angular';
import {HttpService} from "./httpService";
import { FCM } from '@ionic-native/fcm';
import {Injectable} from "@angular/core";
import { LocalNotifications } from '@ionic-native/local-notifications';
import { HTTPResponse } from '@ionic-native/http';

@Injectable()
/** Класс для работы с push нотификациями сервиса firebase*/
export class FCMService{
  notificationid: number = 1;
  constructor(public platform: Platform, private http: HttpService, private fcm: FCM, private localNotifications: LocalNotifications) {

  }

  /**
   * Инициализирует работу с нотификациями. Пытается получить токен и отправить его на сервер, а так же подписывается на пример нотификаций и отображает их
   */
  initNotifications() {
    if(!this.platform.is('cordova'))
        return;
    this.platform.ready().then(() => {
      this.fcm.getToken().then(token => {
        console.log(token);
        this.registerToken(token);
      });

      this.fcm.onNotification().subscribe(data => {
        console.log(data)
        if(data.wasTapped) {
          console.info("Received in background");
        } else {
          console.info("Received in foreground");
          let text = data.text;
          this.localNotifications.schedule({
              id: this.notificationid,
              text: text,
              vibrate: true,
              launch: true,
              icon: 'fcm_push_icon',
              smallIcon: 'fcm_push_icon'
          });
          this.notificationid++;
        };
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token)
        this.registerToken(token);
      });
      this.localNotifications.on('click').subscribe(function(clickData) {
        console.log('CLICK');
        console.log(clickData)
      });
    });
  }
  
  /**
   * Отправляет полученный токен на сервер
   */
  registerToken(token) {
    let body = {
      "token": token
    };
    this.http.postData('/account/registerfcmtoken', body)
      .then((res: HTTPResponse) => {
        console.log(res.data);
      })
      .catch((err: HTTPResponse) => {
        console.log("error");
        console.log(err.error);
      });
  }
}
