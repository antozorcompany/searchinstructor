import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import { Keyboard} from '@ionic-native/keyboard'
import { HttpClientModule } from '@angular/common/http'
import { NativeHttpModule } from 'ionic-native-http-connection-backend';
import { CallNumber } from '@ionic-native/call-number'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http'
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { TextMaskModule } from 'angular2-text-mask';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FCM } from '@ionic-native/fcm';
import { BackgroundMode } from '@ionic-native/background-mode';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { HTTP } from '@ionic-native/http';

import { MyApp } from './app.component';
import { UserTypeSelectPage } from '../pages/usertypeselect/usertypeselect';
import { StudentStartPage } from '../pages/studentstart/studentstart';
import { AutorizationsPage } from '../pages/autorizations/autorizations';
import { RegistrationstudentPage} from '../pages/registrationStudent/registrationstudent'
import { RegistratIoninstructor1Page } from '../pages/registrationInstructor1/registrationinstructor1'
import { RegistrationInstructor2Page } from '../pages/registrationInstruktor2/registrationinstructor2'
import { RegistrationInstructor3Page } from '../pages/registrationInstruktor3/registrationinstructor3'
import { SearchinstructorPage} from '../pages/searchinstructor/searchinstructor'
import { StudentAuthPage} from '../pages/studentauth/studentauth'
import { InstructorAuthPage} from '../pages/instructorauth/instructorauth'
import { ResultSearchPage } from '../pages/resultsearch/resultseartch'
import { EndRegistrationInstructor } from '../pages/endRegistrationInstructor/endRegistrationInstructor';
import { EndRegistrationStudent } from '../pages/endRegistrationStudent/endRegistrationStudent';
import { ProfileInstructor } from '../pages/profileInstructor/profileInstructor';
import { ProfileInstructorForStudent } from '../pages/profileInstructorForStudent/profileInstructorForStudent'
import { EditProfileInstructor  } from '../pages/editProfileInstructor/editProfileInstructor'
import { CheckPhone} from '../pages/checkPhone/checkPhone'
import { RequestChangePassword} from '../pages/requestChangePassword/requestChangePassword'
import { StartPage } from '../pages/startPage/startPage'
import { ChangePassword} from '../pages/changePassword/changePassword'
import { AddAuto } from '../modules/addAuto/addAuto'
import { Select } from '../modules/select/select'
import { AfterIf } from '../modules/afterIf/afterIf'
import { Customheader } from '../modules/customHeader/customheader'
import { ValidationWindows } from './validationWindows'
import { QuestionWindow } from './questionWindow';
import { CommentWindows } from './commentWindow'
import { PhotoSelectorWindow } from './photoSelectorWindow';
import { GlobalProvider } from './globalSettingsProvider'
import { Comments } from '../pages/comments/comments'
import { Comment } from '../pages/comment/comment'
import { NeedUpdatePage } from '../pages/needupdate/needupdate'

import {HttpService} from "./httpService";
import {FCMService} from "./fcmService";
import { CarEditView } from '../pages/carEditView/carEditView'

@NgModule({
  declarations: [
    MyApp,
    UserTypeSelectPage,
    StudentStartPage,
    AutorizationsPage,
    RegistrationstudentPage,
    RegistratIoninstructor1Page,
    RegistrationInstructor2Page,
    RegistrationInstructor3Page,
    SearchinstructorPage,
    StudentAuthPage,
    InstructorAuthPage,
    ResultSearchPage,
    EndRegistrationInstructor,
    EndRegistrationStudent,
    ProfileInstructor,
    ProfileInstructorForStudent,
    EditProfileInstructor,
    CheckPhone,
    RequestChangePassword,
    ChangePassword,
    Select,
    AddAuto,
    Customheader,
    StartPage,
    CarEditView,
    Comments,
    Comment,
    NeedUpdatePage,
    AfterIf
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NativeHttpModule,
    TextMaskModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      animate: false,
      scrollAssist: true, 
      autoFocusAssist: false,
      scrollPadding: false
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UserTypeSelectPage,
    StudentStartPage,
    AutorizationsPage,
    RegistrationstudentPage,
    RegistratIoninstructor1Page,
    RegistrationInstructor2Page,
    RegistrationInstructor3Page,
    SearchinstructorPage,
    StudentAuthPage,
    InstructorAuthPage,
    ResultSearchPage,
    EndRegistrationInstructor,
    EndRegistrationStudent,
    ProfileInstructor,
    ProfileInstructorForStudent,
    EditProfileInstructor,
    CheckPhone,
    RequestChangePassword,
    ChangePassword,
    Select,
    AddAuto,
    Customheader,
    StartPage,
    CarEditView,
    Comments,
    Comment,
    NeedUpdatePage
  ],
  providers: [
    Keyboard,
    StatusBar,
    SplashScreen,
    HttpService,
    ValidationWindows,
    CommentWindows,
    QuestionWindow,
    PhotoSelectorWindow,
    CallNumber,
    Geolocation,
    NativeGeocoder,
    Camera,
    Crop,
    HTTP,
    File,
    FileTransfer,
    FCM,
    FCMService,
    BackgroundMode,
    LocalNotifications,
    AndroidPermissions,
    GlobalProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    // {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
  ]
})
export class AppModule {
}
