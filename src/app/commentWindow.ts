import {Injectable} from "@angular/core";


@Injectable()
/** Заготовка класса компонента для вывода пользователю сообщения об необходимости оставить отзыв перед простановкой лайка и двумя кнопками. Проблема в том что html код лежит в app.html а текст захардкоден там же. */
export class CommentWindows {
  private context: any = null;
  private okCallback: any = null;
  private cancelCallback: any = null;

  constructor() {

  }
  showComment(okCallback, cancelCallback, context){
    this.okCallback = okCallback;
    this.cancelCallback = cancelCallback;
    this.context = context;

    let popup = document.getElementById('popup-comment');
    popup.style.display = "block";
    let popup_button_cancel = document.getElementById('popup-comment__button_cancel');
    popup_button_cancel.addEventListener('click',  this.onEventClickCancel);
    let popup_button_ok = document.getElementById('popup-comment__button_ok');
    popup_button_ok.addEventListener('click',  this.onEventClickOk);
  }
  private onEventClickOk = () => {
    this.onClick(this.okCallback, this.context);
  }
  private onEventClickCancel = () => {
    this.onClick(this.cancelCallback, this.context);
  }
  private onClick(callback, context){
    let popup = document.getElementById('popup-comment');
    popup.style.display = "none";
    let popup_button_cancel = document.getElementById('popup-comment__button_cancel');
    popup_button_cancel.removeEventListener('click',  this.onEventClickCancel);
    let popup_button_ok = document.getElementById('popup-comment__button_ok');
    popup_button_ok.removeEventListener('click',  this.onEventClickOk);
    if(callback)
      callback.call(context);
  }
}
