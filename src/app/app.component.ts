import { Component, ViewChild, Renderer2 } from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackgroundMode } from '@ionic-native/background-mode';
import { HTTPResponse } from '@ionic-native/http';

import { UserTypeSelectPage } from '../pages/usertypeselect/usertypeselect';
import {HttpService} from "./httpService";
import {StudentAuthPage} from "../pages/studentauth/studentauth";
import {InstructorAuthPage} from "../pages/instructorauth/instructorauth";
import { NeedUpdatePage } from '../pages/needupdate/needupdate'
import {FCMService} from "./fcmService";

@Component({
  templateUrl: 'app.html'
})

/** Основной класс приложения с которого начинается его работа. */
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  root: any;
  validationItem: string ='';
  validetionShow: boolean = false;
  /**
   * Начинает работу приложения. В зависимости от авторизованности пользователя либо показывает
   * приветственное окно либо стратовое окно с выбором типа пользователя.
   */
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    private http: HttpService, private fcm: FCMService, private backgroundMode: BackgroundMode,
    private renderer2: Renderer2) {
      this.initializeApp();
    }

    initializeApp() {
      this.platform.ready().then(() => {
        //if(!this.backgroundMode.isEnabled())
        //  this.backgroundMode.enable()
        // Оказалось что надо скрыть все найденные кнопки а не только последнюю так как иногда текущая
        // видимая кнопка не всегда является последней. Например кнопка "Сохранить" при редактировании
        // машин в профиле инструктора
        window.addEventListener('keyboardWillShow', () => {
          let buttonsBlock = document.getElementsByClassName('buttons_block');
          for(let i = 0; i < buttonsBlock.length; i++) {
            let cl = buttonsBlock[i].classList;
            if(cl.contains('regInstr3Buttons') || cl.contains('carEditButtons'))
              continue;
            this.renderer2.addClass(buttonsBlock[i], 'showHide');
          }
        });
        window.addEventListener('keyboardWillHide', () => {
          let buttonsBlock = document.getElementsByClassName('buttons_block');
          for(let i = 0; i < buttonsBlock.length; i++) {
            let cl = buttonsBlock[i].classList;
            if(cl.contains('regInstr3Buttons') || cl.contains('carEditButtons'))
              continue;
            this.renderer2.removeClass(buttonsBlock[i], 'showHide');
          }
        });
        this.statusBar.styleDefault();
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString('#5580A3');
        if(localStorage.getItem('Session') == 'instructor' || localStorage.getItem('Session') == 'student') {
          this.http.getData('/login?v=1&t=' + (new Date()).getTime())
            .then((res: HTTPResponse) => {
              let data = JSON.parse(res.data);
              if (data.status == 'ok') {
                this.fcm.initNotifications();
                if (data.typeSession == 'instructor') {
                  this.nav.setRoot(InstructorAuthPage, {
                    'username': data.username,
                    'views_count': data.views_count
                  });
                } else if (data.typeSession == 'student') {
                  this.nav.setRoot(StudentAuthPage, {
                    'username': data.username
                  });
                }
              }
            })
            .catch((err: HTTPResponse) => {
              console.log("error");
              console.log(err);
              let text = err.error;
              if(text == "wrong version")
                this.root = NeedUpdatePage;
              else
                this.root = UserTypeSelectPage;
            });
        }
        else {
          this.root = UserTypeSelectPage;
        }
      });
    }
    public validation(validationText){
      this.validationItem = validationText;
      this.validetionShow = true;
    }

}
