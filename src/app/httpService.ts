import {Injectable} from "@angular/core";
import { HTTP, HTTPResponse } from '@ionic-native/http';


@Injectable()
/** Класс для отправки get и post запросов на север. */
export class HttpService{
  // public urlServer:string = 'http://localhost:3000';
  public urlServer:string = 'http://31.31.192.219:80';
  // public urlServer:string = 'http://192.168.1.5:3000';
  constructor(private http: HTTP) {
  }

  postData(url, body): Promise<HTTPResponse> {
    this.http.setDataSerializer('json')
    return this.http.post(this.urlServer+url, body, {});
  }
  getData(url): Promise<HTTPResponse> {
    return this.http.get(this.urlServer+url, {}, {});
  }
}
