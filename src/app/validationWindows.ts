import {Injectable} from "@angular/core";


@Injectable()
/** Заготовка класса компонента для вывода сообщения об ошибке пользователю. Сообщение с одной кнопкой ОК. Проблема в том что html код лежит в app.html */
export class ValidationWindows {
  constructor() {

  }
  showValidate(text, callback=null, context=null){
    let popup = document.getElementById('popup-validation');
    popup.style.display = "block";
    let popup_button = document.getElementById('popup-validation__button');
    popup_button.addEventListener('click',  this.onEventClick.bind(null, popup, callback, context));
    if(text != undefined){
      let title = document.getElementById('popup-validation__title');
      title.innerText = text
    }
  }
  private onEventClick(popup, callback, context){
    popup.style.display = "none";
    if(callback)
      callback.call(context);
  }
  //Вызывается при отсутствии интернета
  showConnectionLostMessage() {
    this.showValidate('Отсутствует соединение с сервером. Попробуйте позже.');
  }
  //Вызывается для кода 500
  showServerErrorMessage() {
    this.showValidate('Произошла ошибка на сервере. Попробуйте позже.');
  }
  showUnknownErrorMessage() {
    this.showValidate('Произошла неизвестная ошибка. Попробуйте позже.');
  }

}
