import {Injectable} from "@angular/core";


@Injectable()
/** Заготовка класса компонента для вывода пользователю вопроса с двумя кнопками. Проблема в том что html код лежит в app.html. */
export class QuestionWindow {
  private context: any = null;
  private yesCallback: any = null;
  private noCallback: any = null;

  constructor() {

  }
  showQuestion(text, yesCallback, noCallback, context){
    this.yesCallback = yesCallback;
    this.noCallback = noCallback;
    this.context = context;

    let popup = document.getElementById('popup-question');
    if(text != undefined){
      let title = document.getElementById('popup-question__title');
      title.innerText = text
    }
    popup.style.display = "block";
    let popup_button_no = document.getElementById('popup-question__button_no');
    popup_button_no.addEventListener('click',  this.noClick);
    let popup_button_yes = document.getElementById('popup-question__button_yes');
    popup_button_yes.addEventListener('click',  this.yesClick);
  }
  private yesClick = (): void => {
    this.onClick(this.yesCallback, this.context);
  }
  private noClick = (): void => {
    this.onClick(this.noCallback, this.context);
  }
  private onClick(callback, context){
    let popup = document.getElementById('popup-question');
    popup.style.display = "none";
    let popup_button_no = document.getElementById('popup-question__button_no');
    popup_button_no.removeEventListener('click',  this.noClick);
    let popup_button_yes = document.getElementById('popup-question__button_yes');
    popup_button_yes.removeEventListener('click',  this.yesClick);
    if(callback)
      callback.call(context);
  }
}
