import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController } from 'ionic-angular';
import {Injectable} from "@angular/core";

@Injectable()
/** Класс с набором глобальных функций используемых в разных местах*/
export class GlobalProvider {
  myspinner: string = "<object style='animation: spinner 1s infinite linear; width:36px; height:36px;' data='assets/imgs/spinner.svg' type='image/svg+xml'></object>";
  constructor(private sanitizer: DomSanitizer, private loadingCtrl: LoadingController) {

  }
  /**
   * Создает и настраивает компонент для вывода колеса ожидания выполнения операции
   * @return {LoadingController} компонент ожидания
   */
  createLoadingController() {
    let loading = this.loadingCtrl.create({spinner:'hide'});
    loading.data.content = this.sanitizer.bypassSecurityTrustHtml(this.myspinner);
    return loading;
  }
  
  /**
   * Возвращает правильный формат слова в зависимости от года
   * @param {number} year - год
   * @return {string} год/года/лет
   */
  getYearFormat(year) {
    let m = year % 10;
    if(year >= 5 && year <= 20)
      return 'лет';
    if(m == 1)
      return 'год';
    if(m == 2 || m == 3 || m == 4)
      return 'года';
    return 'лет'
  }
  
  /**
   * Возвращает правильный формат слова в зависимости от количества отзывов
   * @param {number} count - количество отзывов
   * @return {string} отзыв/отзыва/отзывов
   */
  getReviewFormat(count) {
    let m = count % 10;
    if(count >= 5 && count <= 20)
      return 'отзывов';
    if(m == 1)
      return 'отзыв';
    if(m == 2 || m == 3 || m == 4)
      return 'отзыва';
    return 'отзывов'
  }
}
