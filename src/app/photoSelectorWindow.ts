import {Injectable} from "@angular/core";


@Injectable()
/** Заготовка класса компонента для вывода пользователю диалога с вопросом где взять фотографию. */
export class PhotoSelectorWindow {
  private context: any = null;
  private galaryCallback: any = null;
  private cameraCallback: any = null;
  
  constructor() {

  }
  showWindow(galaryCallback, cameraCallback, context){
    this.galaryCallback = galaryCallback;
    this.cameraCallback = cameraCallback;
    this.context = context;
    
    let popup = document.getElementById('popup-photosource');
    popup.style.display = "block";
    let popup_button_galary = document.getElementById('popup-photosource__button_galary');
    popup_button_galary.addEventListener('click',  this.galaryClick);
    let popup_button_camera = document.getElementById('popup-photosource__button_camera');
    popup_button_camera.addEventListener('click',  this.cameraClick);
    let popup_button_cancel = document.getElementById('popup-photosource__button_cancel');
    popup_button_cancel.addEventListener('click',  this.cancelClick);
  }
  private galaryClick = (): void => {
    this.onClick(this.galaryCallback, this.context);
  }
  private cameraClick = (): void => {
    this.onClick(this.cameraCallback, this.context);
  }
  private cancelClick = (): void => {
    this.onClick(null, null);
  }
  private onClick(callback, context){
    let popup = document.getElementById('popup-photosource');
    popup.style.display = "none";
    let popup_button_galary = document.getElementById('popup-photosource__button_galary');
    popup_button_galary.removeEventListener('click',  this.galaryClick);
    let popup_button_camera = document.getElementById('popup-photosource__button_camera');
    popup_button_camera.removeEventListener('click',  this.cameraClick);
    let popup_button_cancel = document.getElementById('popup-photosource__button_cancel');
    popup_button_cancel.removeEventListener('click',  this.cancelClick);
    if(callback)
      callback.call(context);
  }
}
