import {Component, ElementRef, EventEmitter, Input, Output, Renderer, SimpleChanges, ViewChild} from '@angular/core';
import {Renderer2} from "@angular/core";
import { Platform } from 'ionic-angular';

@Component({
  selector: 'selectcustom',
  templateUrl: 'select.html'
})
export class Select {
  clickCheck: boolean = false;
  showValue: boolean = false;
  @Input() items: string;
  @Input() title: string;
  @Input() id: string;
  @Input() icon: string;
  @Input() alias: string;
  @Input() findSymbol: string;
  @Input() searchtitle: string;
  @ViewChild('searchinput') searchInput: ElementRef;
  arrayItems: any; // Полный исходный набор всех элементов
  showFindSymbol: boolean;
  showItems: any; // Набор элементов которые показываются в текущий момент
  resultItems: any; // Набор элементов которые должны быть показаны по мере прокрутки
  selectedItems: string;
  page: number = 1; // Текущая отображаемая страница
  itemsOnPage = 100; // Количество элементов на странице
  checkSearch: boolean = false;
  showingText: string = '';
  unregisterBackButtonAction: any;

  constructor(private renderer: Renderer2, private renderer2: Renderer, private platform: Platform) {
  }
  showFunc(){
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.hideFunc();
    }, 100);
    this.clickCheck = !this.clickCheck;
    let headerElement = document.getElementsByTagName('customheader');
    let header = headerElement.item(headerElement.length-1);
    this.renderer.addClass(header,'showHide');
  }
  hideFunc(){
    this.unregisterBackButtonAction();
    this.clickCheck = !this.clickCheck;
    let headerElement = document.getElementsByTagName('customheader');
    let header = headerElement.item(headerElement.length-1);
    this.renderer.removeClass(header,'showHide');
    this.checkSearch = false;
    this.page = 1;
    this.resultItems = this.arrayItems;
    this.showItems = this.resultItems.slice(0, this.itemsOnPage);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.showFindSymbol = this.findSymbol != undefined;
    if(this.alias != '' && this.alias != undefined) {
      this.showValue = true;
      this.showingText = this.alias;
    }
    this.arrayItems = JSON.parse(this.items);
    if(this.id != undefined){
      for(let i = 0; i < this.arrayItems.length; i++){
        if(this.arrayItems[i]['id'] == this.id){
          this.selectedItems = this.arrayItems[i].id;
          if(this.showingText == '' || this.showingText == this.title) {
            this.showingText = this.arrayItems[i].title;
            this.alias = this.arrayItems[i].title;
          }
          this.showValue = true;
        }
      }
    }
    if(this.showingText == '')
      this.showingText = this.title;
    this.page = 1;
    this.resultItems = this.arrayItems;
    this.showItems = this.resultItems.slice(0, this.itemsOnPage);
    if (!this.searchtitle)
      this.searchtitle = this.title;
  }
  @Output() onChanged = new EventEmitter<string>();
  selected(id) {
    this.selectedItems = id.id;
    this.showingText = id.title;
    this.alias = id.title;
    this.showValue = true;
    this.onChanged.emit(id.id);
    setTimeout( this.hideFunc.bind(this), 100);
   
  }
  rightButton(){
    this.checkSearch = !this.checkSearch;
  }
   valuechange(input){
    if(input.target.value == '') {
      this.page = 1;
      this.resultItems = this.arrayItems;
      this.showItems = this.resultItems.slice(0, this.itemsOnPage);
    }
    else {
      this.page = 1;
      this.resultItems = this.search(this.arrayItems, input.target.value);
      this.showItems = this.resultItems.slice(0, this.itemsOnPage);
    }
  }
  search(array, str){
    let resultArray = [];
    for(let a in array){
      for (let i = 0; i < str.length; i++){
        if (array[a].title.length >= str.length){
          if (array[a]['title'][i].toLowerCase() != str[i].toLowerCase()) {
            break;
          }
          else {
            if (i == str.length - 1) {
              resultArray.push(array[a]);
            }
          }
        }
      }
    }
    return resultArray
  }
  afterIf(input){
    input.innerText=''
    this.renderer2.invokeElementMethod(input, 'focus');
  }
  scroll = (): void => {
    let scrollBlock = document.getElementsByClassName('selectedItem')[0];
    if(scrollBlock.scrollHeight - window.innerHeight - scrollBlock.scrollTop < 100) {
      if(this.page * this.itemsOnPage < this.resultItems.length) {
        this.page += 1;
        this.showItems = this.resultItems.slice(0, this.page * this.itemsOnPage);
      }
    }
  };
}

