import {Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'customheader',
  templateUrl: 'customheader.html'
})
export class Customheader {
  @Input() title: string;
  @Input() rightButtonSymbol: string;
  @Input() backButtonSymbol: string = '';
  @Input() middleButtonSymbol: string;
  @Input() showBackButton: string = 'true';
  rightButtonVisibility: boolean = false;
  backButtonVisibility: boolean = true;
  middleButtonVisibility: boolean = false;
  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.rightButtonVisibility = !(this.rightButtonSymbol == undefined || this.rightButtonSymbol == '');
    this.backButtonVisibility = this.showBackButton != 'false';
    this.middleButtonVisibility = !(this.middleButtonSymbol == undefined || this.middleButtonSymbol == '');
  }
  @Output() onBack = new EventEmitter();
  back() {
    this.onBack.emit();
  }
  @Output() onRightButton = new EventEmitter();
  rightButton() {
    this.onRightButton.emit();
  }
  @Output() onMiddleButton = new EventEmitter();
  middleButton(){
    this.onMiddleButton.emit();
  }
}
