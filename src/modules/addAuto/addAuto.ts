import {Component, EventEmitter, Input, Output, SimpleChanges, ViewChild, Renderer2} from '@angular/core';
import {Nav, NavController, NavParams, normalizeURL} from "ionic-angular";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import {PhotoSelectorWindow} from "../../app/photoSelectorWindow";
import { Platform } from 'ionic-angular';

export class Car {
  id: number;
  name: string;
  vehicle_type: string;
  gearbox: string;
  manufacturer: string;
  photo: string;
  immideatelyEdit: boolean;

  constructor(id=null, name=null, vehicle_type=null, gearbox=null, manufacturer=null, photo=null) {
    this.id = id;
    this.name = name;
    this.vehicle_type = vehicle_type;
    this.gearbox  = gearbox;
    this.manufacturer = manufacturer;
    this.photo = photo;

    this.immideatelyEdit = false;
  }

  toJson() {
    return {
      'id': this.id,
      'name':this.name,
      'vehicle_type': this.vehicle_type,
      'gearbox': this.gearbox,
      'manufacturer': this.manufacturer,
      'photo': this.photo
    }
  }
};

@Component({
  selector: 'add-auto',
  templateUrl: 'addAuto.html'
})
export class AddAuto {
  @ViewChild(Nav) nav: Nav;

  @Input() id: number;
  @Input() name: string;
  @Input() vehicle_type: string;
  @Input() gearbox: string;
  @Input() manufacturer: string;
  @Input() photo: string;
  // Дикий костыль чтобы избавится от "мерцания" при переходе к редактированию первого авто. Не скрывает
  // возможный заголовок окна со списком машин так как его точно нет
  @Input() firstCarEdit: string = 'false';

  @Input('editMode') editModeParam: string;
  @Input() immideatelyEdit: string;
  @Input('title') editorTitle: string = 'Регистрация';

  @Output() onCreate = new EventEmitter<number>();
  @Output() onChangeAuto = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<number>();
  
  editorsVisibility: boolean = false;
  editMode: boolean = false;
  deletingCarMode : boolean = false; //костыль так как похоже что нет stopPropogation
  gearTitle: any = {'mechanic': 'Механическая', 'automatic':'Автоматическая'};
  gearInput: string;
  categoriesInput: string;
  manufacturerInput: string;
  buttonText: string = 'Добавить автомобиль';
  car: Car;
  editCar: Car;

  typeEdit: boolean = false;
  categories: string = JSON.stringify([
    {'title':'A','id':'A'},
    {'title':'B','id':'B'},
    {'title':'C','id':'C'},
    {'title':'D','id':'D'},
    {'title':'E','id':'E'}
  ]);
  gearboxes: string = JSON.stringify([
    {'title':'Механическая','id':'mechanic'},
    {'title':'Автоматическая','id':'automatic'}
  ]);
  manufacturers: string = JSON.stringify([
    {'title':'Отечественная','id':'russian'},
    {'title':'Иномарка','id':'foreign'}
  ]);
  defaultImageSrc: string = "assets/imgs/fotik.svg";
  imageSrc: SafeUrl = this.defaultImageSrc;
  unregisterBackButtonAction: any;

  constructor(private renderer: Renderer2, public navCtrl: NavController, private camera: Camera,
    private photoSelectorWindow: PhotoSelectorWindow, private ds: DomSanitizer, public navParams: NavParams,
    private platform: Platform, private crop: Crop, private renderer2: Renderer2) {
  }
  showEditors(){
    if(this.editMode && !this.deletingCarMode) {
      if(this.editCar.photo == "assets/imgs/tachka.svg")
        this.imageSrc = this.defaultImageSrc;
      this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
        this.closeFunc();
      }, 100);
      this.fillEditors();
      this.editorsVisibility = !this.editorsVisibility;
      //Скороем кнопки иначе на иосе они остануться видны
      let buttons = document.getElementsByClassName('carEditButtons');
      if(buttons.length > 0)
        this.renderer2.addClass(buttons[0], 'showHide');
      buttons = document.getElementsByClassName('regInstr3Buttons');
      if(buttons.length > 0)
        this.renderer2.addClass(buttons[0], 'showHide');
      // Сделаем видимым кастомный заголовок редактирования
      if(this.firstCarEdit == 'false') {
        let headerElement = document.getElementsByTagName('customheader');
        for (let i = headerElement.length - 1; i >= 0; i--) {
          if (headerElement.item(i).classList[0] != 'add-auto-header') {
            this.renderer.addClass(headerElement.item(i), 'showHide');
          }
        }
      }
    }
  }
  deleteCar() {
    this.deletingCarMode = true;
    this.onDelete.emit(this.id);
  }
  closeFunc(){
    this.editCar = Object.assign(new Car(), this.car);
    // Функция скрытия вызывается только если редактируется не первое авто при регистрации. Если эту функцию
    // вызвать в таком случае то будет видно мерцание так как идет возврат на 2 окна назад
    if(this.firstCarEdit == 'false')
      this.hideEditors();
    if(!this.isValid()) {
      // Если при закрытии редактора получилась невалидная машина значит мы не закончили ввод параметров
      // и передумали. Значит машину можно удалить
      this.unregisterBackButtonAction();
      this.deleteCar();
      return;
    }
    if(this.imageSrc == this.defaultImageSrc)
      this.imageSrc = this.ds.bypassSecurityTrustUrl(this.car.photo);
  }
  hideEditors(){
    this.unregisterBackButtonAction();
    this.editorsVisibility = !this.editorsVisibility;
    //Скроем кастомный заголовок редактирования
    if(this.firstCarEdit == 'false') {
      // Вернем их обратно
      let buttons = document.getElementsByClassName('carEditButtons');
      if(buttons.length > 0)
        this.renderer2.removeClass(buttons[0], 'showHide');
      buttons = document.getElementsByClassName('regInstr3Buttons');
      if(buttons.length > 0)
        this.renderer2.removeClass(buttons[0], 'showHide');
      let headerElement = document.getElementsByTagName('customheader');
      for (let i  = headerElement.length-1; i >= 0; i--){
        if (headerElement.item(i).classList[0] != 'add-auto-header'){
          this.renderer.removeClass(headerElement.item(i),'showHide');
        }
      }
    }
  }
  addAuto(){
    if(this.isValid()){
      this.car = Object.assign(new Car(), this.editCar);
      this.onChangeAuto.emit(this.car);
      this.hideEditors()
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.car = new Car(this.id, this.name, this.vehicle_type, this.gearbox, this.manufacturer, this.photo);
    this.editCar = new Car(this.id, this.name, this.vehicle_type, this.gearbox, this.manufacturer, this.photo);

    this.imageSrc = this.ds.bypassSecurityTrustUrl(this.car.photo);
    this.editMode = (this.editModeParam == 'true');
    
    if(this.immideatelyEdit == 'true') {
      this.showEditors();
    }

    if(this.editMode) {
      this.buttonText = 'Сохранить';
    }
  }
  fillEditors() {
    this.gearInput = this.car.gearbox;
    this.manufacturerInput = this.car.manufacturer;
    this.categoriesInput = this.car.vehicle_type;
  }
  onChangedManufacturer(selectedValue: any) {
    this.editCar.manufacturer = selectedValue;
  }
  onChangedGearbox(selectedValue: any) {
    this.editCar.gearbox = selectedValue;
  }
  onChangedCategory(selectedValue: any) {
    this.editCar.vehicle_type = selectedValue;
  }
  isValid() {
    return this.editCar.manufacturer && this.editCar.gearbox && this.editCar.vehicle_type && this.editCar.name;
  }
  valuechange(input) {
    this.editCar.name = input.target.value;
  }
  selectPhotoType() {
    this.photoSelectorWindow.showWindow(this.selectFromGalary, this.makePhoto, this);
  }
  selectFromGalary() {
    this.selectPhoto(true);
  }
  makePhoto() {
    this.selectPhoto(false);
  }
  selectPhoto(galary:boolean) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.BACK,
      correctOrientation: true
    };
    if(galary)
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.camera.getPicture(options)
      .then((file_uri) => {
        this.crop.crop(file_uri, { quality: 100, targetWidth: -1, targetHeight: -1 })
          .then((path) => {
            path = normalizeURL(path);
            this.imageSrc = this.ds.bypassSecurityTrustUrl(path);
            this.editCar.photo = path;
          })
      })
  }
}
